# Challenge-Chapter5
## Project status

This challenge is incomplete. There's need some work on the CRUD mechanism. The only thing working is loading the data. Hopefully i could finish it ASAP.

## Challenge 5

In this challenge 5, there's 2 repository. This is the repository for the views, and the second one is [the API Repo](https://gitlab.com/luminerrr/api-carsdb)

## Library Used
This node.js project uses several libraries. The list of libraries used
    - [Express.js](https://expressjs.com/)
        Express is used to build the http server.
    - [Axios](https://axios-http.com/docs/intro)
        Axios is used to fetch the data from API.
    - [EJS](https://ejs.co/)
        EJS is used for the templating engine.
    - [Nodemon](https://nodemon.io/)
        Nodemon is used to re-run server when saved.

## How to use it

Clone the repository, and then run these
```
npm install
node server/index.js

```

## Entity Relationship Diagram

Theres only 1 table in the DB. [Click here](https://dbdiagram.io/d/62657e541072ae0b6adc07e0) too see the ERD
