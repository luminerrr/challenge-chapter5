// Required Modules
const axios = require('axios');
const express = require('express');
const app = express();
const PORT = 3000;
const path = require('path');
const fs = require('fs');
const { type } = require('os');

// Public path for static files
const PUBLIC_PATH = path.join(__dirname, '../public');
const VIEW_PATH = path.join(__dirname,'../views');

app.set('views', VIEW_PATH);
app.set('view engine','ejs');
app.use(express.json());
app.use(express.static(PUBLIC_PATH));

// console.log('View Path ', VIEW_PATH)
// console.log('public path ', PUBLIC_PATH)

// Main page
app.get('/', (req,res) =>{
    const Cars = axios.get('http://localhost:3001/cars').then(cars =>{
        res.render('index', {data : cars.data.data})
    })
});

// Edit
app.get('/edit', (req,res)=>{
    res.render('edit');
})


// Add
app.post('/cars-added', (req,res,next) =>{
    res.status(200).json(req.body);
    next();
})

app.get('/add', (req,res)=>{
    res.render('add')
})

app.listen(PORT, () =>{
    console.log(`Server listening on ${PORT}, go to http://localhost:${PORT}`)
});